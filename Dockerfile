# PHP 7.1 Apache as Base
FROM php:7.1-apache

# Update Core PHP extension.
RUN apt-get update -yqq
RUN apt-get install git libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq

# Compile PHP, include these extensions.
RUN docker-php-ext-install mbstring mcrypt pdo_mysql curl json intl gd xml zip bz2 opcache

# Install and enable mongodb extension
RUN pecl install mongodb
RUN docker-php-ext-enable mongodb

# Install composer
RUN curl -sS https://getcomposer.org/installer | php

# Copy apache pre-configuration files
COPY config/apache2.conf /etc/apache2/
COPY config/sites-available/ /etc/apache2/sites-available/

# Set the default document root
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
# For the APACHE_ALIAS_PATH configuration
# It will simulate the 'Alias' command to
# 'APACHE_DOCUMENT_ROOT' by using rewrite mod
# useful when use in 'subdirectory proxying'
#
# if '/' is passed, disable 'Alias'
# Example: '/alias/path'
ENV APACHE_ALIAS_PATH /

# Enable mod_rewrite and mod_headers
RUN a2enmod rewrite
RUN a2enmod headers

# Copy php.ini
COPY config/php.ini /usr/local/etc/php/

# Copy application into document root
COPY application/ /var/www/html/

# Create storage symlink directory (public_html/storage <--> storage/app/public)
RUN ln -sr /var/www/html/storage/app/public /var/www/html/public/storage

# Run permission for storage
RUN chown -R www-data:www-data /var/www/html/storage
RUN chmod -R 777 /var/www/html/storage

# Mark public storage for external volume
VOLUME /var/www/html/storage/app/public
